import sys
import urllib
from multiprocessing import Pool, cpu_count

"""
Create a download manager, that will download html page by http url and
save it to the file in the background. Download can't block and doesn't need
to support a way to get its result. (hint: use can use threads and httplib)

Example usage:

dm = DownloadManager()
dm.download('http://www.pythonforbeginners.com/', 'pb.html')
dm.download('https://howtodoinjava.com', 'java.html')
dm.download('https://stackoverflow.com', 'so.html')
"""


class DownloadManager(object):
    def __init__(self):
        self.process_pool = Pool(maxtasksperchild=1, processes=cpu_count())

    def download(self, url, file_path):
        self.process_pool.apply_async(urllib.urlretrieve, [url, file_path])

    def __del__(self):
        self.process_pool.close()
        self.process_pool.join()


def main():
    dm = DownloadManager()
    dm.download('http://www.pythonforbeginners.com/', 'pb.html')
    dm.download('https://howtodoinjava.com', 'java.html')
    dm.download('https://stackoverflow.com', 'so.html')

if __name__ == "__main__":
    main()
    sys.exit(0)