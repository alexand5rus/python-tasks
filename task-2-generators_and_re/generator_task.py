import sys
import re

"""
Create a Python's generator, that will yield the links and
their descriptions found in http document.

Example usage:

http_doc = ('
<!DOCTYPE html>
<html>
<body>

<h2>Python links</h2>

<a href="https://www.why-i-love.python.com">Reasons why I love Python</a>
<a href="python4-docs.com" title="Python4">Documentation for Python 4</a>
<a href="/more_links">More links about Python</a>

</body>
</html>
')

links = list(find_links(http_doc))
assert(links == [
    ('Reasons why I love Python', 'https://www.why-i-love.python.com'),
    ('Documentation for Python 4', 'python4-docs.com'),
    ('More links about Python', '/more_links'),
])
"""


http_doc = ('''
<!DOCTYPE html>
<html>
<body>

<h2>Python links</h2>

<a href="https://www.why-i-love.python.com">Reasons why I love Python</a>
<a href="python4-docs.com" title="Python4">Documentation for Python 4</a>
<a href="/more_links">More links about Python</a>

</body>
</html>
''')


link_name_pattern = re.compile(r'<a href=\"(?P<link>.+?)\".*>(?P<name>.+?)</a>')

def find_links(text):
    match = link_name_pattern.search(text)
    while match:
        name = match.group('name')
        link = match.group('link')
        match = link_name_pattern.search(http_doc, match.end())
        yield name, link

def main():
    links = list(find_links(http_doc))
    assert (links == [
        ('Reasons why I love Python', 'https://www.why-i-love.python.com'),
        ('Documentation for Python 4', 'python4-docs.com'),
        ('More links about Python', '/more_links'),
    ])
    return 0


if __name__ == '__main__':
    sys.exit(main())