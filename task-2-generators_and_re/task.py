#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create a Python's generator, that will yield the links and
their descriptions found in http document.

Example usage:

http_doc = ('
<!DOCTYPE html>
<html>
<body>

<h2>Python links</h2>

<a href="https://www.why-i-love.python.com">Reasons why I love Python</a>
<a href="python4-docs.com" title="Python4">Documentation for Python 4</a>
<a href="/more_links">More links about Python</a>

</body>
</html>
')

links = list(find_links(http_doc))
assert(links == [
    ('Reasons why I love Python', 'https://www.why-i-love.python.com'),
    ('Documentation for Python 4', 'python4-docs.com'),
    ('More links about Python', '/more_links'),
])
"""
