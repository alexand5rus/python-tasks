#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create a Python's decorator that will limit funtion call to N times a second,
where N is an argument of the decorator

Example usage:

@limit(5)
def spammer():
    print("I will spam this message to stdout if no decorator stops me!!!!!")

while True:
    spammer()

In this example message has to be printed only 5 times per second
"""
