import sys
import time
from functools import wraps


"""
Create a Python's decorator that will limit funtion call to N times a second,
where N is an argument of the decorator

Example usage:

@limit(5)
def spammer():
    print("I will spam this message to stdout if no decorator stops me!!!!!")

while True:
    spammer()

In this example message has to be printed only 5 times per second
"""

def limit(limit_count):
    def func_wrapper(func):
        func_wrapper.count = limit_count
        func_wrapper.start_time = time.time()
        @wraps(func)
        def limited_func():
            if int(time.time()) - func_wrapper.start_time > 1:
                func_wrapper.count = limit_count
                func_wrapper.start_time = time.time()
            if func_wrapper.count > 0:
                func()
            func_wrapper.count -= 1

        return limited_func
    return func_wrapper

@limit(5)
def spammer():
    print("I will spam this message to stdout if no decorator stops me!!!!!")


@limit(10)
def spamm():
    print("+++++++++++++++++++++++++")


def main():
    while True:
        spammer()
        spamm()
    return 0

if __name__ == '__main__':
    sys.exit(main())