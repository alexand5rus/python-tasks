#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Given a n*n matrix where all numbers are distinct.
Find the maximum length path (starting from any cell) such that all cells
along the path are in increasing order with a difference of 1.

We can move in 4 directions from a given cell (i, j), i.e., we can move
to (i+1, j) or (i, j+1) or (i-1, j) or (i, j-1) with the condition that
the adjacent cells have a difference of 1.
"""


def find_longest(matrix):
    return []


assert(find_longest([[1, 2, 9],
                     [5, 3, 8],
                     [4, 6, 7]]) == [6, 7, 8, 9])
