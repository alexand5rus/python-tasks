import sys

"""
Given a n*n matrix where all numbers are distinct.
Find the maximum length path (starting from any cell) such that all cells
along the path are in increasing order with a difference of 1.

We can move in 4 directions from a given cell (i, j), i.e., we can move
to (i+1, j) or (i, j+1) or (i-1, j) or (i, j-1) with the condition that
the adjacent cells have a difference of 1.
"""

def find_longest(matrix):
    mass = []
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            mass.append((matrix[i][j], (i, j)))

    mass.sort()

    def check_coordinates(coordinate1, coordinate2):
        if (coordinate1[0] - coordinate2[0] == -1 or coordinate1[0] - coordinate2[0] == 1) and coordinate1[1] == coordinate2[1]:
            return True
        if (coordinate1[1] - coordinate2[1] == -1 or coordinate1[1] - coordinate2[1] == 1) and coordinate1[0] == coordinate2[0]:
            return True
        return False

    max_path = []
    tmp_path = []

    for i in range(len(mass)):
        if i < len(mass) - 1:
            if check_coordinates(mass[i][1], mass[i+1][1]):
                tmp_path.append(mass[i][0])
                continue
            tmp_path.append(mass[i][0])

            if len(tmp_path) >= len(max_path):
                max_path = tmp_path

            tmp_path = []
            continue
        tmp_path.append(mass[i][0])

    if len(tmp_path) >= len(max_path):
        max_path = tmp_path

    return max_path


def main():
    assert (find_longest([[1, 2, 9],
                          [5, 3, 8],
                          [4, 6, 7]]) == [6, 7, 8, 9])
    return 0

if __name__ == '__main__':
    sys.exit(main())