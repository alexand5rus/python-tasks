#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create a class Graph, that represents undirected unwaighted graph and supports such operations:
    1. Read undirected graph from DOT file (en.wikipedia.org/wiki/DOT_(graph_description_language)).
       You can implement parser yourself (only the most basic case) or use an existing library
    2. Generator method, that traverses graph breadth-first
    3. Generator method, that traverses graph depth-first
    4. Method, that checks if all nodes in the list are connected. Nodes are represented by node name
    5. Method, that checks if all nodes in the list are a part of the same cycle. Nodes are represented by node name

Add unit tests for all Graph methods

Note: Graph may not be connected and may contain cycles
"""


# you can change interface if you want
class Graph(object):
    def __init__(json_file):
        pass

    def breadth_search(self):
        yield

    def depth_search(self):
        yield

    def are_connected(nodes_list):
        return False

    def are_same_cycle(nodes_list):
        return False
