import sys
import re

from collections import deque

"""
Create a class Graph, that represents undirected unwaighted graph and supports such operations:
    1. Read undirected graph from DOT file (en.wikipedia.org/wiki/DOT_(graph_description_language)).
       You can implement parser yourself (only the most basic case) or use an existing library
    2. Generator method, that traverses graph breadth-first
    3. Generator method, that traverses graph depth-first
    4. Method, that checks if all nodes in the list are connected. Nodes are represented by node name
    5. Method, that checks if all nodes in the list are a part of the same cycle. Nodes are represented by node name

Add unit tests for all Graph methods

Note: Graph may not be connected and may contain cycles
"""

edges_pattern = re.compile(r'(?P<node1>.+?) -- (?P<node2>.+?);')

class Graph(object):
    def __init__(self, source_file):
        self.nodes = {}
        with open(source_file, 'r') as f:
            text = f.read()

        match = edges_pattern.search(text)
        while match:
            node1 = match.group('node1')
            node2 = match.group('node2')
            self._fill_nodes(node1, node2)
            self._fill_nodes(node2, node1)
            match = edges_pattern.search(text, match.end())

    def breadth_search(self, root):
        return self._search(deque, self._popqueue, root)

    def depth_search(self, root):
        return self._search(list, self._popstack, root)

    def are_connected(self, nodes_list):
        cont = [nodes_list[0]]
        checked_list = []
        while cont:
            root = cont.pop()
            checked_list.append(root)
            for i in self.nodes[root]:
                if i not in checked_list and i not in cont and i in nodes_list:
                    cont.append(i)

        if len(checked_list) == len(nodes_list):
            return True

        return False

    def are_same_cycle(self, nodes_list):
        return False

    def _fill_nodes(self, node1, node2):
        if node1 in self.nodes.keys():
            self.nodes[node1].append(node2)
        else:
            self.nodes[node1] = [node2]

    def _search(self, container, pop_method, root):
        cont = container([root])
        checked_list = []
        while cont:
            root = pop_method(cont)
            checked_list.append(root)
            yield root
            for i in self.nodes[root]:
                if i not in checked_list and i not in cont:
                    cont.append(i)

    def _popqueue(self, container):
        return container.popleft()

    def _popstack(self, container):
        return container.pop()



def main():
    g = Graph('./graph.dot')
    for i in g.breadth_search('m'):
        print i

    print('+++++++++++++++++++++')

    for i in g.depth_search('m'):
        print i

    print('+++++++++++++++++++++')

    print('connected = {}'.format(g.are_connected(['m', 'f'])))


if __name__ == '__main__':
    sys.exit(main())